$(function(){

	function Page() {

		this.el = $('#canvas');
		this.ctx =  $('#canvas')[0].getContext("2d");
		this.height =  $("#canvas").height();
		this.width = $("#canvas").width();
		this.minX = $("#canvas").offset().left;
		this.maxX = this.minX + this.width;
		this.minY = $("#canvas").offset().top;
		this.maxY = this.minY + this.height;

		//законы физики для нашей страницы
		this.g = 9.81;
		this.k_energy = 0.75; // коэфициет потери энергии
		this.dt = 0.1; // шаг
		
	};



	function Ball( x, y, radius, color ){
		this.r = radius;
		this.x = x;
		this.y = y;
		this.color = color;
		this.h = null; // высота падения ( масштаб 1/100 )
		this.v = null; // скорость
		this.timer = null // таймер
		this.pause = false;

		// рисуем шар
		this.circle = function() {
			myPage.ctx.clearRect( 0, 0, myPage.width, myPage.height );
			myPage.ctx.fillStyle = this.color;
			myPage.ctx.beginPath();

			myPage.ctx.arc( this.x, this.y, this.r, 0, Math.PI*2, true );
			myPage.ctx.closePath();
			myPage.ctx.fill();
		};


		// вычисляем параметры для шага
		this.calculation = function () {
			if ( this.h  <= 1 ){
				this.v = -this.v * myPage.k_energy;
				if ( Math.abs(this.v) < 0.5 ){  
					clearInterval(this.timer);  
					console.log(this)                
				}
			}

			this.v += myPage.g * myPage.dt;
			this.h -= this.v * myPage.dt;

			this.y = myPage.height - this.r  - Math.round( this.h );

		};


		this.draw = function() {
			this.circle(this.x, this.y, 10);
			this.calculation();	     
		};

		this.init = function() {
			this.v = null;
			this.h = (myPage.height - this.y);
			this.calculation();
			this.timer = setInterval($.proxy(this.draw, this), 10);
		};

		this.mouseDown = function(evt) {
			if (evt.pageX > myPage.minX && evt.pageX < myPage.maxX) {
				var mouse_x = evt.pageX - myPage.minX;
				var mouse_y = evt.pageY - myPage.minY;
				if ( this.x - this.r <= mouse_x && this.x + this.r >=  mouse_x ) {
			  		if ( this.y - this.r <= mouse_y && this.y + this.r >=  mouse_y ) {
			  			this.pause = true;
			  			clearInterval(this.timer);
			  		}
			  	}
			}
		};

		this.mouseUp = function (evt) {
			var mouse_x = evt.pageX - myPage.minX;
			var mouse_y = evt.pageY - myPage.minY;
			myball.x = mouse_x;
			myball.y = mouse_y;
			myball.pause = false; 
			myball.init();
		};

		this.mouseMove = function (evt) {

			var mouse_x = evt.pageX - myPage.minX;
			var mouse_y = evt.pageY - myPage.minY;

			if ( this.pause === true ) {

				this.x = mouse_x;
				this.y = mouse_y;

				this.draw();
			}
		};



		$(myPage.el).on( 'mousedown', $.proxy(this.mouseDown, this) );
		$(myPage.el).on( 'mouseup', $.proxy(this.mouseUp, this) );
		$(myPage.el).on( 'mousemove', $.proxy(this.mouseMove, this) );

		this.init();
	};


	var myPage = new Page();
	var myball = new Ball(200, 0, 30, 'red');

});